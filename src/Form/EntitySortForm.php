<?php

namespace Drupal\entity_sort\Form;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler to sort entities.
 *
 * @internal
 */
class EntitySortForm extends FormBase implements FormInterface {

  use DependencySerializationTrait;

  /**
   * The batch size of update.
   */
  const BATCH_SIZE = 20;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The weight field.
   *
   * @var string
   */
  protected $weightKey;

  /**
   * The entity type.
   *
   * @var \Drupal\Core\Entity\EntityType
   */
  protected $entityType;

  /**
   * The entity type ID.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * The entity bundle ID.
   *
   * @var string
   */
  protected $bundleId;

  /**
   * The entities.
   *
   * @var \Drupal\Core\Entity\EntityInterface[]
   */
  protected $entities;

  /**
   * Constructs a form object.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(ConfigFactory $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_sort_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type = NULL, $bundle = NULL) {
    $this->setEntityTypeId($entity_type)->setBundleId($bundle)->loadEntities();
    $form['entities'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#empty' => t('There are no @label yet.', ['@label' => $this->entityType()->getPluralLabel()]),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'weight',
        ],
      ],
    ];
    $delta = 10;
    // Change the delta of the weight field if have more than 20 entities.
    if (!empty($this->weightKey())) {
      $count = count($this->entities);
      if ($count > 20) {
        $delta = ceil($count / 2);
      }
    }
    foreach ($this->entities as $entity) {
      $row = $this->buildRow($entity);
      if (isset($row['label'])) {
        $row['label'] = ['#plain_text' => $row['label']];
      }
      if (isset($row['weight'])) {
        $row['weight']['#delta'] = $delta;
      }
      $form['entities'][$entity->id()] = $row;
    }

    if (count($this->entities)) {
      $form['actions']['#type'] = 'actions';
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => t('Save'),
        '#button_type' => 'primary',
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // No validation.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity_weight_list = $form_state->getValue('entities');
    if (count($this->entities) > self::BATCH_SIZE) {
      $batch_builder = (new BatchBuilder())
        ->addOperation('_entity_sort_update_batch_process', [$this->entities, $entity_weight_list, $this->weightKey()])
        ->setFinishCallback('_entity_sort_update_batch_finished')
        ->setTitle($this->t('Processing'))
        ->setErrorMessage($this->t('The update has encountered an error.'))
        ->setProgressMessage('');
      batch_set($batch_builder->toArray());
    }
    else {
      foreach ($entity_weight_list as $id => $value) {
        if (isset($this->entities[$id]) && $this->entities[$id]->get($this->weightKey()) != $value['weight']) {
          // Save entity only when its weight was changed.
          $this->entities[$id]->set($this->weightKey(), $value['weight']);
          $this->entities[$id]->save();
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['title'] = $this->t('Title');
    if (!empty($this->weightKey())) {
      $header['weight'] = t('Weight');
    }
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    /** @var \Drupal\Core\Entity\ContentEntityBase */
    if (!$entity->hasField($this->weightKey())) {
      return $row;
    }

    $row['title'] = [
      '#type' => 'link',
      '#title' => $entity->getTitle(),
      '#url' => $entity->toUrl(),
    ];
    // Override default values to markup elements.
    $row['#attributes']['class'][] = 'draggable';
    $row['#weight'] = $entity->get($this->weightKey())->value;
    // Add weight column.
    $row['weight'] = [
      '#type' => 'weight',
      '#title' => t('Weight for @title', ['@title' => $entity->label()]),
      '#title_display' => 'invisible',
      '#default_value' => $entity->get($this->weightKey())->value,
      '#attributes' => ['class' => ['weight']],
    ];
    return $row;
  }

  /**
   * Loads entities.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadEntities() {
    if (!$this->entities) {
      $storage = $this->entityTypeManager->getStorage($this->entityTypeId);
      $query = $storage->getQuery()->sort($this->weightKey());
      if ($this->bundleId) {
        $query->condition('type', $this->bundleId);
      }
      $this->entities = $storage->loadMultiple($query->execute());
    }
    return $this->entities;
  }

  /**
   * Provides the entity type definition.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface|null
   *   The entity type.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function entityType() {
    if (!$this->entityType) {
      $this->entityType = $this->entityTypeManager->getDefinition($this->entityTypeId);
    }
    return $this->entityType;
  }

  /**
   * Sets entity type ID.
   *
   * @param string $entity_type_id
   *   The entity type.
   *
   * @return $this
   */
  protected function setEntityTypeId($entity_type_id) {
    $this->entityTypeId = $entity_type_id;
    return $this;
  }

  /**
   * Sets entity bundle ID.
   *
   * @param string $bundle_id
   *   The entity bundle.
   *
   * @return $this
   */
  protected function setBundleId($bundle_id) {
    $this->bundleId = $bundle_id;
    return $this;
  }

  /**
   * Gets weight key.
   *
   * @return null|string
   *   Weight field name.
   */
  protected function weightKey() {
    if (!$this->weightKey) {
      $weight_field_settings = $this->configFactory->get('entity_sort.settings')->get('weight_fields');
      $this->weightKey = $weight_field_settings[$this->entityTypeId][$this->bundleId];
    }
    return $this->weightKey;
  }

}
